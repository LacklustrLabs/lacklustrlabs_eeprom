#include "Arduino.h"
#include "lacklustrlabs_eeprom.h"

#if defined(ARDUINO_ARCH_STM32F1)
#define SOUT Serial1
#else
#define SOUT Serial
#endif

using namespace lacklustrlabs;

eeprom::ProgramIdentifier pi;

void setup() {
  SOUT.begin(115200);
  //delay(1000);
  
  SOUT.println(F("Starting EEPROM example "));

  char data2[] = {"this is a test."};
  pi.put(0,data2);
  SOUT.print("wrote:");
  SOUT.println(data2);

  char data3[] = {"               "};
  pi.get(0,data3);
  SOUT.print("read:");
  SOUT.println(data3);

  uint16_t data1 = 0xf00d;
  pi.put(0,data1);
  SOUT.print("wrote:");
  SOUT.println(data1,HEX);
  data1 =0;
  
  pi.get(0,data1);
  SOUT.print("read:");
  SOUT.println(data1,HEX);


}

void loop() {
  exit(0);
}

