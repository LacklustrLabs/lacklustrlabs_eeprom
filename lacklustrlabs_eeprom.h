#pragma once

#include "Arduino.h"
#include "EEPROM.h"

namespace lacklustrlabs {
namespace eeprom {

#if defined(ARDUINO_ARCH_STM32F1)
// stmduino eeprom does not have the same C++ API as arduino 1.6.12

struct ProgramIdentifier {
  uint16_t programId;
  uint16_t version;

  //Functionality to 'get' and 'put' objects to and from EEPROM.

  // This operates on whole 16 bit words
  template< typename T > T &get( uint16_t idx, T &t ){
    uint16_t *ptr = (uint16_t*) &t;
    // we are writing in whole 16 bit words, but sizeof(T) is measuring bytes.
    for( size_t count = 0; count< sizeof(T)/2 ; count++ ) {
      EEPROM.read(idx, ptr);
      ptr++;
      idx++;
    }
    return t;
  }

  template< typename T > const T &put( uint16_t idx, const T &t ){
    uint16_t *ptr = (uint16_t*) &t;
    // we are reading whole 16 bit words, but sizeof(T) is measuring bytes.
    for( size_t count = 0; count< sizeof(T)/2 ; count++ ) {
      // simulate update()
      if (EEPROM.read(idx) != ptr[idx]) {
        EEPROM.write(idx, ptr[idx]);
      }
      idx++;
    }
    return t;
  }

};

#else

struct ProgramIdentifier {
  uint16_t programId;
  uint16_t version;
  
  //Functionality to 'get' and 'put' objects to and from EEPROM.
  template< typename T > 
  static T &get( int idx, T &t ){
    return EEPROM.get(idx, t);
  }
  
  template< typename T > 
  static const T &put( int idx, const T &t ){
    return EEPROM.put(idx, t);
  }
};

#endif

}
}
